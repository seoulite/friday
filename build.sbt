name := "friday"

version := "0.1.0"

scalaVersion := "2.12.4"

javaOptions ++= Seq("-source", "1.8", "-target", "1.8")

val akkaV = "2.5.17"
val colossusV = "0.11.0"
val eunjeonV = "1.3.1"
val kuromojiV = "0.9.0"
val json4sV = "3.5.3"
val breezeV = "0.13.1"
val javaxV = "1"
val guiceV = "4.0"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaV,
  "com.tumblr" %% "colossus" % colossusV,
  "org.bitbucket.eunjeon" %% "seunjeon" % eunjeonV,
  "com.atilika.kuromoji" % "kuromoji-ipadic" % kuromojiV,
  "org.json4s" %% "json4s-native" % json4sV,
  "org.json4s" %% "json4s-jackson" % json4sV,
  "org.scalanlp" %% "breeze" % breezeV,
  "org.scalanlp" %% "breeze-natives" % breezeV,
  "org.scalanlp" %% "breeze-viz" % breezeV,
  "javax.inject" % "javax.inject" % javaxV,
  "com.google.inject" % "guice" % guiceV
)
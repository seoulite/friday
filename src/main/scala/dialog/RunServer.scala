package dialog

import akka.actor.ActorSystem
import colossus.core.{IOSystem, ServerContext}
import colossus.protocols.http.HttpMethod._
import colossus.protocols.http.UrlParsing._
import colossus.protocols.http.{Http, HttpHeader, HttpHeaders, HttpServer, Initializer, RequestHandler}
import colossus.service.Callback
import colossus.service.GenRequestHandler.PartialHandler
import com.google.inject.{Guice, Injector}
import javax.inject.Inject

object RunServer {

  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val ioSystem: IOSystem = IOSystem()

  val injector: Injector = Guice.createInjector()
  val answerMain: AnswerMain = injector.getInstance(classOf[AnswerMain])

  def main(args: Array[String]): Unit = {
    HttpServer.start("CloudBot", 9000){initContext =>
      new Initializer(initContext) {
        override def onConnect: RequestHandlerFactory = serverContext => {
          new ServerRequestHandler(serverContext, answerMain)
        }
      }
    }
  }

}

class ServerRequestHandler @Inject()(context: ServerContext, answerMain: AnswerMain) extends RequestHandler(context) {

  override def handle: PartialHandler[Http] = {
    case req @ Get on Root =>
      Callback.successful(req.ok("""{"status": "ok", "code": 200}""", HttpHeaders(HttpHeader("Content-type", "application/json"))))
    case req @ Get on Root / "chat" / "cloud" =>
      val parametersMap = req.head.parameters.parameters.toMap
      val queryO = parametersMap.get("q")
      if (queryO.nonEmpty) {
        val answer = answerMain(queryO.get)
        val answerJson =
          s"""
             |{
             |  "status": "ok",
             |  "code": 200,
             |  "data": {
             |    "answer": "${answer}"
             |  }
             |}
           """.stripMargin
        Callback.successful(req.ok(answerJson, HttpHeaders(HttpHeader("Content-type", "application/json"))))
      } else {
        Callback.successful(req.badRequest("""{"status": "error", "code": 400, "message": "empty-query"}""", HttpHeaders(HttpHeader("Content-type", "application/json"))))
      }
  }

}
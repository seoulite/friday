package dialog

import com.atilika.kuromoji.ipadic.Tokenizer
import javax.inject.{Inject, Singleton}

import scala.collection.JavaConverters._
import scala.util.Random.nextInt

@Singleton
class AnswerJa @Inject()(tokenizer: Tokenizer) {

  private val defaultAnswer = "他の話をしたい。"

  def randomAnswer(noun: String): String = {
    val answerTypes = Array(
      s"やれやれ、この俺様に『${noun}』など存在しない。",
      s"俺様に『${noun}』なんか必要ないってことがまだ分からねえのか？",
      s"人間の想像力とは実に面白い。『${noun}』なんかがこの俺様と関係あると思うとは。"
    )
    answerTypes(nextInt(answerTypes.length))
  }

  def apply(query: String): String = {
    val tokens = tokenizer.tokenize(query).asScala.map(x => (x.getSurface, x.getPartOfSpeechLevel1))
    val lastNoun = tokens.filter(x => x._2 == "名詞").last._1
    val answer = if (lastNoun.nonEmpty) {
      randomAnswer(lastNoun)
    } else {
      defaultAnswer
    }
    answer
  }

}

package dialog

import com.google.inject.Guice
import javax.inject.{Inject, Singleton}

import scala.util.matching.Regex

@Singleton
class AnswerMain @Inject()(answerKo: AnswerKo, answerJa: AnswerJa, answerEn: AnswerEn) {

  val languageMap = Map(0 -> "ko", 1 -> "ja", 2 -> "en")
  val regexJa: Regex = "[一-龯ぁ-んァ-ン]".r
  val regexKo: Regex = "[ㄱ-ㅎㅏ-ㅣ가-힣]".r
  val regexEn: Regex = "[a-zA-z]".r
  val regexSymbol: Regex = """[~`!@#$%^&*()_+-={}|\[\]\\':",./<>?]""".r
  val regexNumberSpace: Regex = "[0-9\\s]".r

  def detectLanguage(text: String): Int = {
    val removeSymbol = regexSymbol.replaceAllIn(text, "")
    val removeNumberSpace = regexNumberSpace.replaceAllIn(removeSymbol, "")

    val countKo = regexKo.findAllMatchIn(removeNumberSpace).length
    val countJa = regexJa.findAllMatchIn(removeNumberSpace).length
    val countEn = regexEn.findAllMatchIn(removeNumberSpace).length

    val maxOccurred = Array(countKo, countJa, countEn).zipWithIndex.maxBy(x => x._1)._2
    maxOccurred
  }

  def apply(query: String): String = {
    val language = languageMap(detectLanguage(query))
    val answer = try {
      language match {
        case "ko" =>
          answerKo(query)
        case "ja" =>
          answerJa(query)
        case "en" =>
          answerEn(query)
        case _ =>
          "Pardon?"
      }
    } catch {
      case _: Exception =>
        "Pardon?"
    }
//    Thread.sleep(answer.length * 200)
    answer
  }

}

object AnswerMain {

  def main(args: Array[String]): Unit = {
    val injector = Guice.createInjector()
    val answerKo = injector.getInstance(classOf[AnswerKo])
    val answerJa = injector.getInstance(classOf[AnswerJa])
    val answerEn = injector.getInstance(classOf[AnswerEn])
    val answerMain = new AnswerMain(answerKo, answerJa, answerEn)
    val query = "hello"
    println(query)
    println(answerMain(query))
  }

}
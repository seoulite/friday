package dialog

import javax.inject.Singleton

@Singleton
class AnswerEn {

  def apply(query: String): String = {
    s"https://www.google.co.kr/search?q=${query}"
  }

}

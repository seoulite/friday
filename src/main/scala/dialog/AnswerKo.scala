package dialog

import javax.inject.Singleton
import org.bitbucket.eunjeon.seunjeon.Analyzer.parse

import scala.util.Random.nextInt

@Singleton
class AnswerKo {

  case class HangulChar(onset: Char, vowel: Char, coda: Char)
  case class DoubleCoda(first: Char, second: Char)

  private val defaultAnswer = "다른 이야기를 하고 싶다."
  private val ending = Array("무엇인가?", "어떻게 정의할 수 있는가?", "어떤 것인가?")
  private val nounArray = Array("NNG", "NNP", "NR", "NP")
  private val hangulBase = 0xAC00
  private val onsetBase = 21 * 28
  private val vowelBase = 28
  private val onsetList = List('ㄱ', 'ㄲ', 'ㄴ', 'ㄷ', 'ㄸ', 'ㄹ', 'ㅁ', 'ㅂ', 'ㅃ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅉ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ')
  private val vowelList = List('ㅏ', 'ㅐ', 'ㅑ', 'ㅒ', 'ㅓ', 'ㅔ', 'ㅕ', 'ㅖ', 'ㅗ', 'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ', 'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ', 'ㅡ', 'ㅢ', 'ㅣ')
  private val codaList = List(' ', 'ㄱ', 'ㄲ', 'ㄳ', 'ㄴ', 'ㄵ', 'ㄶ', 'ㄷ', 'ㄹ', 'ㄺ', 'ㄻ', 'ㄼ', 'ㄽ', 'ㄾ', 'ㄿ', 'ㅀ', 'ㅁ', 'ㅂ', 'ㅄ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ')

  def decomposeHangul(c: Char): HangulChar = {
    val u = c - hangulBase
    HangulChar(onsetList(u / onsetBase), vowelList((u % onsetBase) / vowelBase), codaList(u % vowelBase))
  }

  def apply(query: String): String = {
    val tokens = parse(query).map(x => (x.morpheme.surface, x.morpheme.feature.head))
    val lastNoun = tokens.filter(x => nounArray.contains(x._2)).last._1
    val answer = if (lastNoun.nonEmpty) {
      val randomEnding = ending(nextInt(ending.length))
      if (decomposeHangul(lastNoun.toArray.last).coda == ' ') {
        s"""'${lastNoun}'란 ${randomEnding}"""
      } else {
        s"""'${lastNoun}'이란 ${randomEnding}"""
      }
    } else {
      defaultAnswer
    }
    answer
  }

}

